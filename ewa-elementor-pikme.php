<?php
/**
 * Plugin Name: EWA Elementor Pikme
 * Description: Custom Elementor extension which includes custom widgets for Pikme theme.
 * Plugin URI:  https://essentialwebapps.com/
 * Version:     1.0.2
 * Author:      EWA
 * Author URI:  https://essentialwebapps.com/
 * Text Domain: ewa-elementor-pikme
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * EWA Elementor Pikme Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class Elementor_EWA_Pikme_Extension {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.0';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var Elementor_EWA_Pikme_Extension The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 *
	 * @return Elementor_EWA_Pikme_Extension An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'init', [ $this, 'i18n' ] );
		add_action( 'plugins_loaded', [ $this, 'init' ] );

	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( 'ewa-elementor-pikme', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );

	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
			return;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return;
		}


		// Add Plugin actions
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );
		add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );


		// added by EWA - Register Widget Styles
		add_action('elementor/frontend/after_enqueue_styles', [ $this, 'widget_styles' ] );

		// added by EWA - Register Widget Scripts
		add_action('elementor/frontend/after_enqueue_scripts', [ $this, 'widget_scripts' ] );

        // added by EWA - action hook for 'ewa-pikme, EWA Elements' custom category for panel widgets
		add_action( 'elementor/init', [ $this, 'ewa_pikme_category' ] );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'ewa-elementor-pikme' ),
			'<strong>' . esc_html__( 'EWA Elementor Pikme', 'ewa-elementor-pikme' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'ewa-elementor-pikme' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'ewa-elementor-pikme' ),
			'<strong>' . esc_html__( 'EWA Elementor Pikme', 'ewa-elementor-pikme' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'ewa-elementor-pikme' ) . '</strong>',
			 self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'ewa-elementor-pikme' ),
			'<strong>' . esc_html__( 'EWA Elementor Pikme', 'ewa-elementor-pikme' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'ewa-elementor-pikme' ) . '</strong>',
			 self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		// added by EWA - holding all widgets file key
		$widgets_file_key = ['pikme-intro', 'pikme-heading', 'pikme-about-me', 'pikme-service', 'pikme-counter', 'pikme-portfolio', 'pikme-post-grid', 'pikme-testimonial', 'pikme-price-plan', 'pikme-skills', 'pikme-exp-edu', 'pikme-process', 'pikme-cta', 'pikme-contact-details'];

		// added by EWA - EWA own widgets files, loading all widget files
		foreach ($widgets_file_key as $widget_file_name) {
			require_once( __DIR__ . '/widgets/ewa-' . $widget_file_name . '-widget.php' );
		}

		// added by EWA - EWA own Register widgets, loading all widget names
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Intro_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Heading_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_About_Me_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Service_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Counter_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Portfolio_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Post_Grid_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Testimonial_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Price_Plan_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Skills_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_EXP_EDU_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Process_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Cta_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \EWA_Pikme_Contact_Details_Widget() );


	}

	/**
	 * Init Controls
	 *
	 * Include controls files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_controls() {

		/*
		* Todo: this block needs to be commented out when the custom control is ready
		*
		*
		// Include Control files
		require_once( __DIR__ . '/controls/test-control.php' );

		// Register control
		\Elementor\Plugin::$instance->controls_manager->register_control( 'control-type-', new \Test_Control() );
		*/

	}

	// added by EWA
	public function widget_styles() {

			// todo: file path needs to replaced by minified one
			wp_register_style( 'ewa-elementor-style', plugins_url( 'style.css', __FILE__ ) );
			wp_enqueue_style('ewa-elementor-style');

	}	

    // added by EWA
	public function widget_scripts() {

			// todo: file path needs to replaced by minified one
		    wp_register_script( 'ewa-elementor-slick-js', plugins_url( 'assets/js/vendor/slick.min.js', __FILE__ ) );
		    wp_register_script( 'ewa-elementor-waypoints-js', plugins_url( 'assets/js/vendor/waypoints.min.js', __FILE__ ) );
		    wp_register_script( 'ewa-elementor-counter-js', plugins_url( 'assets/js/vendor/jquery.counterup.min.js', __FILE__ ) );
			wp_register_script( 'ewa-elementor-script', plugins_url( 'assets/minified/js/scripts.min.js', __FILE__ ) );
			wp_enqueue_script('ewa-elementor-slick-js');
			wp_enqueue_script('ewa-elementor-waypoints-js');
			wp_enqueue_script('ewa-elementor-counter-js');
			wp_enqueue_script('ewa-elementor-script');

	}

    // added by EWA - added a custom category for panel widgets which is added by an action hook at the top 'init'
    public function ewa_pikme_category () {

	   \Elementor\Plugin::$instance->elements_manager->add_category( 
	   	'ewa-pikme',
	   	[
	   		'title' => __( 'EWA Pikme', 'ewa-elementor-pikme' ),
	   		'icon' => 'fa fa-plug', //default icon
	   	],
	   	2 // position
	   );

	}


}

Elementor_EWA_Pikme_Extension::instance();