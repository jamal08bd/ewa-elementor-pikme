<?php
/**
 * EWA Elementor Post Grid Widget.
 *
 * Elementor widget that inserts a porst grid into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Post_Grid_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve post grid widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-post-grid-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve post grid widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Post Grid', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve post grid widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-th';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the post grid widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register post grid widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Post Grid Title
		$this->add_control(
			'ewa_post_grid_title',
			[
				'label' => esc_html__( 'Grid Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter Grid title', 'ewa-elementor-pikme' ),
			]
		);

		// Post Grid Pre Title
		$this->add_control(
			'ewa_post_grid_pre_title',
			[
				'label' => esc_html__( 'Grid Pre Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter Grid Pre title', 'ewa-elementor-pikme' ),
			]
		);

		$this->end_controls_section();

		// Source of the posts
		$this->start_controls_section(
			'source_section',
			[
				'label' => esc_html__( 'Posts Source', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'ewa_posts_from_categories_by_slugs',
			[
				'label' => esc_html__( 'Post from Categories (Enter Category slugs separated by comma)', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'category-slug1, category-slug2', 'ewa-elementor-pikme' ),
			]
		);
		$this->end_controls_section();
		// end of the source of the posts
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Post Grid Title Options
		$this->add_control(
			'ewa_post_grid_title_options',
			[
				'label' => esc_html__( 'Post Grid Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Grid Title Color
		$this->add_control(
			'ewa_post_grid_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .section-heading__title' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Grid Pre Title Color
		$this->add_control(
			'ewa_post_grid_pre_title_color',
			[
				'label' => esc_html__( 'Pre Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .section-heading__titletwo' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Grid Item Options
		$this->add_control(
			'ewa_post_grid_item_options',
			[
				'label' => esc_html__( 'Post Grid Item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Grid Item Background Color
		$this->add_control(
			'ewa_post_grid_item_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#F8FAFB',
				'selectors' => [
					'{{WRAPPER}} .latest-blog__item:after' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Post Grid Item Border
		$this->add_control(
			'ewa_post_grid_item_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#eee;',
				'selectors' => [
					'{{WRAPPER}} .latest-blog__item:after' => 'border: 1px solid {{VALUE}}',
				],
			]
		);
		
		// Post Grid Post Title Options
		$this->add_control(
			'ewa_post_grid_post_title_options',
			[
				'label' => esc_html__( 'Post Grid Post Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Grid Post Title Color
		$this->add_control(
			'ewa_post_grid_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .latest-blog__title a' => 'color: {{VALUE}} !important',
				],
			]
		);
		
		// Post Grid Title Background Color
		$this->add_control(
			'ewa_post_grid_title_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => 'inherit',
				'selectors' => [
					'{{WRAPPER}} .latest-blog__title a' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Post Grid Date Options
		$this->add_control(
			'ewa_post_grid_date_options',
			[
				'label' => esc_html__( 'Post Grid Date', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Grid Date Color
		$this->add_control(
			'ewa_post_grid_date_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .latest-blog__date' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render post grid widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$post_grid_title = $settings['ewa_post_grid_title'];
		$post_grid_pre_title = $settings['ewa_post_grid_pre_title'];
		
		// Post from categories getting category Slug's
		$cat_slugs = $settings['ewa_posts_from_categories_by_slugs'];

        $args = array(
          'posts_per_page' => 3,
          'post_type' => 'post',
          'post_status' => 'publish',
          'ignore_sticky_posts' => 1,
          'category_name' => $cat_slugs,
        );
        $query = new WP_Query($args);

       ?>
		<!-- Repai Guide Start Here -->
			<div class="section-heading">
				<div class="section-heading">
					<h4 class="section-heading__title"><?php echo $post_grid_pre_title;?> <span class="section-heading__titletwo"><?php echo $post_grid_title;?></span></h4>
				</div> <!-- section-heading end here -->			
			</div> <!-- section-heading end here -->
		        <div class="latest-blog">
				    <?php 
			        if ($query->have_posts()) :

	                  while ($query->have_posts()) : $query->the_post(); 

	          	        // get the featured image url
				        $image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
				        // get the post date
				        $post_date = get_the_date('M j, Y');


				        // get the category name and link
			  	        $term_list = wp_get_post_terms(get_the_ID(), 'category', ['fields' => 'all']);

			  	        $cat_name = '';
			 	        $cat_link = '';

			            foreach ($term_list as $term) {
			              if (get_post_meta(get_the_ID(), '_yoast_wpseo_primary_category', true) == $term->term_id) {
			                $cat_name = $term->name;
			                $cat_link = get_term_link($term->term_id);
			               break;
			              }
			              else {
			               $cat_name = $term->name;
			               $cat_link = get_term_link($term->term_id);
			              }
			            }
	                ?>
				            <div class="latest-blog__item">
					            <a href="<?php esc_url(the_permalink()); ?>"> <div class="latest-blog__image" style="background-image: url('<?php echo esc_url($image_url); ?>');"> </div></a>
							    <div class="latest-blog__content">
							        <div class="latest-blog__data">
								        <h4 class="latest-blog__title"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
									    <div class="latest-blog__date">
									        <i class="far fa-clock"></i>
										    <?php echo $post_date; ?>
									    </div>
								    </div>
							    </div> <!-- latest-blog__content end here -->
					        </div> <!-- latest-blog__item end here -->
                    <?php
	  		          endwhile;

	                endif; 
					?>
				</div>				
		<!-- Repai Guide End Here -->
       <?php
	}
}