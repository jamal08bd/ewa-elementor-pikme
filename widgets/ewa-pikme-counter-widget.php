<?php
/**
 * EWA Elementor Counter Widget.
 *
 * Elementor widget that inserts counter into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Counter_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve counter widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-counter-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve counter widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Counter', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve counter widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-money-bill-alt';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the counter widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register counter widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-pikme'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Counter Number
		$this->add_control(
		    'ewa_counter_number',
			[
			    'label' => esc_html__('Number','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Counter Number','ewa-elementor-pikme'),
			]
		);
		
		// Counter Title
		$this->add_control(
		    'ewa_counter_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Counter Description','ewa-elementor-pikme'),
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Counter Options
		$this->add_control(
			'ewa_counter_options',
			[
				'label' => esc_html__( 'Counter Block', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Counter Border
		$this->add_control(
			'ewa_counter_block_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .counter-block' => 'border: 5px solid  {{VALUE}}',
				],
			]
		);
		
		// Counter Item Options
		$this->add_control(
			'ewa_counter_item_options',
			[
				'label' => esc_html__( 'Counter Item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Counter Item Background-color
		$this->add_control(
			'ewa_counter_item_background_color',
			[
				'label' => esc_html__( 'Background-color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .counter-block__item' => 'background-color:  {{VALUE}}',
				],
			]
		);
		
		// Counter Number Options
		$this->add_control(
			'ewa_counter_number_options',
			[
				'label' => esc_html__( 'Counter Number', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Counter Number Color
		$this->add_control(
			'ewa_counter_number_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .counter-block__number' => 'color:  {{VALUE}}',
				],
			]
		);
		
		// Counter Text Options
		$this->add_control(
			'ewa_counter_text_options',
			[
				'label' => esc_html__( 'Counter Text', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Counter Text Color
		$this->add_control(
			'ewa_counter_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .counter-block__text' => 'color:  {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render counter widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$counter_number = $settings['ewa_counter_number'];
		$counter_title = $settings['ewa_counter_title'];

       ?>
		<!-- Counter Area Start Here -->
			<div class="counter-block">
			    <div class="counter-block__item">
				    <h3 class="counter-block__number counter"><?php echo $counter_number; ?></h3>
				    <p class="counter-block__text"><?php echo $counter_title;  ?></p>
				</div> <!-- counter-block__item -->
			</div> <!-- counter-block end here -->
		<!-- Counter Area End Here -->
       <?php
	}
}