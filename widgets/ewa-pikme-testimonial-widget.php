<?php
/**
 * EWA Elementor Testimonials Widget.
 *
 * Elementor widget that inserts testimonials into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Testimonial_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-testimonial-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve testimonial widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Testimonial', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve testimonial widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-quote-left';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the testimonial widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register testimonial widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		// Testimonial Repeater
		$repeater = new \Elementor\Repeater();
		
		// Testimonial Description
		$repeater->add_control(
			'ewa_testimonial_des',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__( 'Enter Description' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Testimonial Name
		$repeater->add_control(
			'ewa_testimonial_name',
			[
				'label' => esc_html__( 'Author Name', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Name' , 'ewa-elementor-pikme' ),
			]
		);

        // Testimonial Title
		$repeater->add_control(
			'ewa_testimonial_title',
			[
				'label' => esc_html__( 'Author Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Title' , 'ewa-elementor-pikme' ),
			]
		);

		//Testimonial Icon Code
		$repeater->add_control(
		    'ewa_testimonial_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-pikme'),
			]
		);

		// Testimonial List
		$this->add_control(
			'ewa_testimonial_list',
			[
				'label' => esc_html__( 'Testimonials List', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_testimonial_des }}}',
			]
		);

		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Testimonial Item Options
		$this->add_control(
			'ewa_testimonial_item_options',
			[
				'label' => esc_html__( 'Testimonial Item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Testimonial Item Background
		$this->add_control(
			'ewa_testimonial_item_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area__item' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Testimonial Item Text Color
		$this->add_control(
			'ewa_testimonial_item_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area__item p' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Testimonial Title Options
		$this->add_control(
			'ewa_testimonial_title_options',
			[
				'label' => esc_html__( 'Testimonial Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Testimonial Title Color
		$this->add_control(
			'ewa_testimonial_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area__title h4 span' => 'color: {{VALUE}}',
				],
			]
		);

		// Testimonial Sub Title Color
		$this->add_control(
			'ewa_testimonial_sub_title_color',
			[
				'label' => esc_html__( 'Subtitle Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area__title h4' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Testimonial Icon Options
		$this->add_control(
			'ewa_testimonial_icon_options',
			[
				'label' => esc_html__( 'Testimonial Icon', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Testimonial Icon Background
		$this->add_control(
			'ewa_testimonial_icon_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area__icon' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Testimonial Icon Color
		$this->add_control(
			'ewa_testimonial_icon_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area__icon i' => 'color: {{VALUE}}',
				],
			]
		);

		// Testimonial Dots Options
		$this->add_control(
			'ewa_testimonial_dots_options',
			[
				'label' => esc_html__( 'Testimonial Dots', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Testimonial Dots Color
		$this->add_control(
			'ewa_testimonial_dots_color',
			[
				'label' => esc_html__( 'Dots Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area .slick-dots li button' => 'border:2px solid {{VALUE}}',
				],
			]
		);

		// Testimonial Dots Active Color
		$this->add_control(
			'ewa_testimonial_dots_active_color',
			[
				'label' => esc_html__( 'Dots Active Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .testimonial-area .slick-dots li.slick-active button::before' => 'background-color:{{VALUE}} !important',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render oEmbtestimonial widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
    ?>
		<!-- Testimonial Start Here -->
		    <div class="testimonial-area">
			    <?php 
				foreach (  $settings['ewa_testimonial_list'] as $item ) { 

					$testimonial_des = $item['ewa_testimonial_des'];
					$testimonial_name = $item['ewa_testimonial_name'];
					$testimonial_title = $item['ewa_testimonial_title'];
					$testimonial_icon = $item['ewa_testimonial_icon_code'];
				?>
				    <div class="testimonial-area__item">
				        <p><?php echo $testimonial_des;?></p>
					    <div class="testimonial-area__title">
					        <h4>-<?php echo $testimonial_name;?>
						        <span><?php echo $testimonial_title;?></span>
						    </h4>
					    </div> <!-- testimonial-area__title end here -->
					    <div class="testimonial-area__icon">
					        <?php echo $testimonial_icon; ?>
					    </div> <!-- testimonial-area__icon end here -->
			        </div> <!-- testimonial-area__item -->
				<?php } ?>
				
			</div> <!-- testimonial-area end here -->
		<!-- Testimonial End Here -->
       <?php
	}
}