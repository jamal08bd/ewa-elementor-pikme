<?php
/**
 * EWA Elementor Services Widget.
 *
 * Elementor widget that inserts services into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Service_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve services widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-service-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve services widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Service', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve services widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-bars';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the services widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register services widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-pikme'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Service Image
		$this->add_control(
		    'ewa_service_image',
			[
			    'label' => esc_html__('Choose Service Image','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Service Title
		$this->add_control(
		    'ewa_service_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Service Title','ewa-elementor-pikme'),
			]
		);
		
		// Service Description
		$this->add_control(
		    'ewa_service_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Service Description','ewa-elementor-pikme'),
			]
		);
		
		// Service Button Text
        $this->add_control(
        	'ewa_service_button_text',
			[
				'label'         => esc_html__('Button Text', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Read More','ewa-elementor-pikme'),
			]
        );
		
		//Service Link
		$this->add_control(
		    'ewa_service_link',
			[
			    'label'         => esc_html__('Service Link','ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Service Title Options
		$this->add_control(
			'ewa_service_title_options',
			[
				'label' => esc_html__( 'Service Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Title Color
		$this->add_control(
			'ewa_service_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .service-block__title a' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Service Description Options
		$this->add_control(
			'ewa_service_des_options',
			[
				'label' => esc_html__( 'Service Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Description Color
		$this->add_control(
			'ewa_service_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .service-block__content p' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Service Button Options
		$this->add_control(
			'ewa_service_btn_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Button Color
		$this->add_control(
			'ewa_service_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .service-block__button' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);

		// Service Title Options
		$this->add_control(
			'ewa_service_title_hover_options',
			[
				'label' => esc_html__( 'Service Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Title Color
		$this->add_control(
			'ewa_service_title_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .service-block__title a:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Service Button Options
		$this->add_control(
			'ewa_service_btn_hover_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Service Button Color
		$this->add_control(
			'ewa_service_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .service-block__button:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$service_image = $settings['ewa_service_image']['url'];
		$service_title = $settings['ewa_service_title'];
		$service_description = $settings['ewa_service_des'];
		$service_btn_text = $settings['ewa_service_button_text'];
		$service_link = $settings['ewa_service_link']['url'];
		
       ?>	
        	
		<!-- Single Service Box Start Here -->
			<div class="service-block">
			    <div class="service-block__item">
				    <div class="service-block__image animation-delay0">
					    <img src="<?php echo $service_image; ?>" />
					</div> <!-- service-block__image end here -->
					<div class="service-block__content">
					    <h3 class="service-block__title"><a href="#"><?php echo $service_title;  ?></a></h3>
						<p><?php echo $service_description;  ?>.</p>
						<a href="<?php echo esc_url($service_link); ?>" class="service-block__button"><?php echo $service_btn_text; ?></a>
					</div> <!-- service-block__content end here -->
				</div> <!-- service-block__item -->
			</div> <!-- service-block end here -->  
		<!-- Single Service Box End Here -->
		<?php
	}
}