<?php
/**
 * EWA Elementor Heading Widget.
 *
 * Elementor widget that inserts a heading into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_EXP_EDU_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-exp-edu-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve heading widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Experience and Education', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-heading';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the heading widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'ewa_edu_heading',
			[
				'label' => esc_html__( 'Edu Heading', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Education', 'ewa-elementor-pikme' ),
			]
		);
		
		// Edu Repeater
		$repeater = new \Elementor\Repeater();
		
		// Edu Title
		$repeater->add_control(
			'ewa_edu_title',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Title' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Edu Subtitle
		$repeater->add_control(
			'ewa_edu_sub',
			[
				'label' => esc_html__( 'Subtitle', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Subtitle' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Edu Description
		$repeater->add_control(
			'ewa_edu_des',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__( 'Enter Description' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Edu List
		$this->add_control(
			'ewa_edu_list',
			[
				'label' => esc_html__( 'Edu List', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_edu_title }}}',
			]
		);


		$this->add_control(
			'ewa_exp_heading',
			[
				'label' => esc_html__( 'Exp Heading', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Experience', 'ewa-elementor-pikme' ),
			]
		);
		
		// Exp Repeater
		$repeater = new \Elementor\Repeater();
		
		// Exp Title
		$repeater->add_control(
			'ewa_exp_title',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Title' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Exp Subtitle
		$repeater->add_control(
			'ewa_exp_sub',
			[
				'label' => esc_html__( 'Subtitle', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Subtitle' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Exp Description
		$repeater->add_control(
			'ewa_exp_des',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__( 'Enter Description' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Exp List
		$this->add_control(
			'ewa_exp_list',
			[
				'label' => esc_html__( 'Exp List', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_exp_title }}}',
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Edu and Exp Title Options
		$this->add_control(
			'ewa_edu_exp_title_options',
			[
				'label' => esc_html__( 'Edu and Exp Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Edu and Exp Title Color
		$this->add_control(
			'ewa_edu_exp_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .resume-block__title' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Item Options
		$this->add_control(
			'ewa_edu_exp_item_options',
			[
				'label' => esc_html__( 'Edu and Exp Item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Edu and Exp Item Background Color
		$this->add_control(
			'ewa_edu_exp_item_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f7f7f7',
				'selectors' => [
					'{{WRAPPER}} .resume-block__item' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Item Border Left Color
		$this->add_control(
			'ewa_edu_exp_item_border_left_color',
			[
				'label' => esc_html__( 'Border-Left Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .resume-block__item' => 'border-left: 2px solid {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Item Border Bottom Color
		$this->add_control(
			'ewa_edu_exp_item_border_bottom_color',
			[
				'label' => esc_html__( 'Border-bottom Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .resume-block__item:not(:last-child)' => 'border-bottom: 1px solid {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Arrow Sign Options
		$this->add_control(
			'ewa_edu_exp_arrow_options',
			[
				'label' => esc_html__( 'Edu and Exp Arrow', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Edu and Exp Arrow Background Color
		$this->add_control(
			'ewa_edu_exp_arrow_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .resume-block__arrow:before' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Arrow Border Color
		$this->add_control(
			'ewa_edu_exp_arrow_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => 'transparent',
				'selectors' => [
					'{{WRAPPER}} .resume-block__arrow:before' => 'border: 8px solid {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Arrow Border Left Color
		$this->add_control(
			'ewa_edu_exp_arrow_border_left_color',
			[
				'label' => esc_html__( 'Border Left Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .resume-block__arrow:after' => 'border-left-color: {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Details Options
		$this->add_control(
			'ewa_edu_exp_details_options',
			[
				'label' => esc_html__( 'Details Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Edu and Exp Details Color
		$this->add_control(
			'ewa_edu_exp_details_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .resume-block__details' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Edu and Exp Description Options
		$this->add_control(
			'ewa_edu_exp_des_options',
			[
				'label' => esc_html__( 'Description Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Edu and Exp Description Color
		$this->add_control(
			'ewa_edu_exp_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .resume-block__description' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render heading widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		// get the main heading for Education and Experience
		$edu_heading = $settings['ewa_edu_heading'];
		$exp_heading = $settings['ewa_exp_heading'];
       ?>
		<!-- Section Title Start Here -->
			
		    <div class="grid">
			    <div class="col-12 col-md-6">
				    <div class="resume-block">
					    <h3 class="resume-block__title"><?php echo $edu_heading;?></h3>
						<?php 
				        foreach (  $settings['ewa_edu_list'] as $item ) { 

					        $edu_title = $item['ewa_edu_title'];
					        $edu_sub = $item['ewa_edu_sub'];
					        $edu_des =  $item['ewa_edu_des'];
				        ?>
						
						    <div class="resume-block__item">
						        <span class="resume-block__arrow"></span>
                                <h5 class="resume-block__subtitle"><?php echo $edu_title;?></h5>
							    <span class="resume-block__details"><?php echo $edu_sub;?></span>
                                <p class="resume-block__description"><?php echo $edu_des;?></p>
                            </div> <!-- resume-block__item end here -->
						<?php } ?>
					</div> <!-- resume-block end here -->
			    </div>
				<div class="col-12 col-md-6">
				    <div class="resume-block">
					    <h3 class="resume-block__title"><?php echo $exp_heading;?></h3>
						<?php 
				        foreach (  $settings['ewa_exp_list'] as $item ) { 

					        $exp_title = $item['ewa_exp_title'];
					        $exp_sub = $item['ewa_exp_sub'];
					        $exp_des =  $item['ewa_exp_des'];
				        ?>
						
						    <div class="resume-block__item">
						        <span class="resume-block__arrow"></span>
                                <h5 class="resume-block__subtitle"><?php echo $exp_title;?></h5>
							    <span class="resume-block__details"><?php echo $exp_sub;?></span>
                                <p class="resume-block__description"><?php echo $exp_des;?></p>
                            </div> <!-- resume-block__item end here -->
						<?php } ?>
					</div> <!-- resume-block end here -->
			    </div>
			</div><!-- grid end here -->
			
		<!-- Section Title End Here -->
	    <?php 
	}
}