<?php
/**
 * EWA Elementor Brand Logo Widget.
 *
 * Elementor widget that inserts a brand logo into the page
 *
 * @since 1.0.0
 */
class EWA_Brand_Logo_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name. 
	 *
	 * Retrieve brand logo widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-brand-logo-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve brand logo widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Brand Logo', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve brand logo widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-band-aid';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the brand logo widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register brand logo widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		?>
		<!-- Our Brand Start Here -->
			<h3>HTML GOES HERE</h3>
		<!-- Our Brand End Here -->
		<?php } 
	}
}