<?php
/**
 * EWA Elementor Portfolio Widget.
 *
 * Elementor widget that inserts services into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Portfolio_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve services widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-portfolio-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve services widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Portfolio', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve services widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-bars';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the services widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register services widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		// Portfolio Block Pre Title
		$this->add_control(
			'ewa_portfolio_pre_title',
			[
				'label' => esc_html__( 'Portfolio Pre Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter pre title', 'ewa-elementor-pikme' ),
			]
		);

		// Portfolio Block Title
		$this->add_control(
			'ewa_portfolio_title',
			[
				'label' => esc_html__( 'Portfolio Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter title', 'ewa-elementor-pikme' ),
			]
		);
		
		$this->end_controls_section();

		// Source of the portfolio
		$this->start_controls_section(
			'source_section',
			[
				'label' => esc_html__( 'Portfolio Source', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'ewa_posts_from_categories_by_slug',
			[
				'label' => esc_html__( 'Portfolio from Categories (Enter Category Slug separated by comma)', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'category-slug1, category-slug2', 'ewa-elementor-pikme' ),
			]
		);
		$this->end_controls_section();
		// end of the source of the posts
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Portfolio Image Overlay Options
		$this->add_control(
			'ewa_image_overlay_options',
			[
				'label' => esc_html__( 'Portfolio Image Overlay', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Portfolio Image Overlay Background Color
		$this->add_control(
			'ewa_portfolio_image_overlay_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => 'rgba(0, 0, 0, 0.4)',
				'selectors' => [
					'{{WRAPPER}} .image-overlay' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Portfolio Image Overlay Link Background Color
		$this->add_control(
			'ewa_portfolio_image_overlay_link_background_color',
			[
				'label' => esc_html__( 'Icon Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .image-overlay a.link' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Portfolio Image Overlay Link Icon Color
		$this->add_control(
			'ewa_portfolio_image_overlay_link_icon_color',
			[
				'label' => esc_html__( 'Link Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .image-overlay a i' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Portfolio Details Options
		$this->add_control(
			'ewa_portfolio_details_options',
			[
				'label' => esc_html__( 'Portfolio Details', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Portfolio Details Border
		$this->add_control(
			'ewa_portfolio_details_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__detali' => 'border: 1px solid {{VALUE}}',
				],
			]
		);

		// Portfolio Details Title
		$this->add_control(
			'ewa_portfolio_details_title',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__title h5 a' => 'color: {{VALUE}}',
				],
			]
		);

		// Portfolio Details Description
		$this->add_control(
			'ewa_portfolio_details_desc',
			[
				'label' => esc_html__( 'Description Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__title p' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Portfolio Title Options
		$this->add_control(
			'ewa_portfolio_title_options',
			[
				'label' => esc_html__( 'Portfolio Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Portfolio Title Color
		$this->add_control(
			'ewa_portfolio_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .section-heading__title' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Portfolio Title Text Color
		$this->add_control(
			'ewa_portfolio_title_text_color',
			[
				'label' => esc_html__( 'Pre Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .section-heading__titletwo' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);

        // Portfolio Item Hover Options
		$this->add_control(
			'ewa_portfolio_item_hover_options',
			[
				'label' => esc_html__( 'Portfolio Item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Portfolio Item Image Overlay Link Hover Background Color
		$this->add_control(
			'ewa_portfolio_item_image_overlay_link_hover_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#FFFFFF',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__item:hover .image-overlay a.link:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

        // Portfolio Item Details Hover Border
		$this->add_control(
			'ewa_portfolio_item_details_hover_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__item:hover .portfolio-block__detali' => 'border: 1px solid {{VALUE}}',
				],
			]
		);

        // Portfolio Item Title Hover Color
		$this->add_control(
			'ewa_portfolio_item_title_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__item:hover .portfolio-block__title h5 a' => 'color: {{VALUE}}',
				],
			]
		);

        // Portfolio Item Title Text Hover Color
		$this->add_control(
			'ewa_portfolio_item_title_text_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .portfolio-block__item:hover .portfolio-block__title p' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Portfolio Image Overlay Link Icon Hover Options
		$this->add_control(
			'ewa_image_overlay_link_icon_hover_options',
			[
				'label' => esc_html__( 'Portfolio Image Overlay', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Portfolio Image Overlay Link Icon Hover Color
		$this->add_control(
			'ewa_portfolio_image_overlay_link_icon_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .image-overlay a i:hover' => 'color: {{VALUE}}',
				],
			]
		);
		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$portfolio_pre_title = $settings['ewa_portfolio_pre_title'];
		$portfolio_title = $settings['ewa_portfolio_title'];
		
		// Gallery from categories getting category ID's
		$cat_slugs = $settings['ewa_posts_from_categories_by_slug'];

        $args = array(
          'slide_to_show' => 4,
          'slide_type' => 'post',
          'slide_status' => 'publish',
          'ignore_sticky_slide' => 1,
          'category_name' => $cat_slugs,
        );
        $query = new WP_Query($args);
		
       ?>	
        	
		<!-- Single Service Box Start Here -->
			<div class="section-heading">
				<div class="section-heading">
					<h4 class="section-heading__title"><?php echo $portfolio_pre_title;?> <span class="section-heading__titletwo"><?php echo $portfolio_title;?></span></h4>
				</div> <!-- section-heading end here -->			
			</div> <!-- section-heading end here -->
			<div class="portfolio-block">
			    <?php 
			    if ($query->have_posts()) :

	               while ($query->have_posts()) : $query->the_post(); 

	          	    // get the featured image url
				    $image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
				    // get the post date
				    $post_date = get_the_date('M j, Y');        


				    // get the category name and link
			  	    $term_list = wp_get_post_terms(get_the_ID(), 'category', ['fields' => 'all']);

			  	    $cat_name = '';
			 	    $cat_link = '';    

			        foreach ($term_list as $term) {
			          if (get_post_meta(get_the_ID(), '_yoast_wpseo_primary_category', true) == $term->term_id) {
			            $cat_name = $term->name;
			            $cat_link = get_term_link($term->term_id);
			           break;
			          }
			          else {
			            $cat_name = $term->name;
			            $cat_link = get_term_link($term->term_id);
			          }
			        }
	            ?>
				    <div class="portfolio-block__item">
				        <div class="portfolio-block__image" style="background-image:url('<?php echo esc_url($image_url); ?>')">
					        <div class="image-overlay">
					            <a class="link" href="<?php esc_url(the_permalink()); ?>">
						            <i class="fas fa-link"></i>
						        </a>    
					        </div>
				        </div> <!-- portfolio-block__image end here -->
				        <div class="portfolio-block__detali">
				            <div class="portfolio-block__title">
					            <h5><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h5>
								<p><?php the_excerpt(); ?></p>
						    </div>
				        </div> <!-- portfolio-block__detali end here -->
				    </div> <!-- portfolio-block__item end here -->
				<?php
	  		        endwhile;

	            endif; 
	            ?>	
			</div> <!-- portfolio-block end here -->
		<!-- Single Service Box End Here -->
		<?php
	}
}