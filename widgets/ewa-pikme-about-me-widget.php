<?php
/**
 * EWA Elementor About Us Widget.
 *
 * Elementor widget that inserts about us into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_About_Me_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve about us widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-about-me-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve about us widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikeme About Me', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve about us widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the about us widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register about us widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-pikme'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// About Me Image
		$this->add_control(
		    'ewa_about_me_image',
			[
			    'label' => esc_html__('Choose About Me Image','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// About Me Facebook Link
		$this->add_control(
        	'ewa_about_me_facebook_link',
			[
				'label'         => esc_html__('Facebook Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		//About Me Facebook Icon Code
		$this->add_control(
		    'ewa_about_me_facebook_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Facebook Icon Code','ewa-elementor-pikme'),
			]
		);
		
		// About Me Twitter Link
		$this->add_control(
        	'ewa_about_Me_twitter_link',
			[
				'label'         => esc_html__('Twitter Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		//About Me Twitter Icon Code
		$this->add_control(
		    'ewa_about_me_twitter_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Twitter Icon Code','ewa-elementor-pikme'),
			]
		);
		
		// About Me Instagram Link
		$this->add_control(
        	'ewa_about_me_instagram_link',
			[
				'label'         => esc_html__('Instagram Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);
		
		//About Me Instagram Icon Code
		$this->add_control(
		    'ewa_about_me_instagram_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Instagram Icon Code','ewa-elementor-pikme'),
			]
		);
		
		// About Me Linkedin Link
		$this->add_control(
        	'ewa_about_me_linkedin_link',
			[
				'label'         => esc_html__('Linkedin Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
		);		
		
		//About Me Linkedin Icon Code
		$this->add_control(
		    'ewa_about_me_linkedin_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Linkedin Icon Code','ewa-elementor-pikme'),
			]
		);
		
		// About Me Subtitle
		$this->add_control(
		    'ewa_about_me_sub',
			[
			    'label' => esc_html__('About Me Subtitle','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter About Me Subtitle','ewa-elementor-pikme'),
			]
		);
		
		// About Me Title
		$this->add_control(
		    'ewa_about_me_title',
			[
			    'label' => esc_html__('About Me Title','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter About Me Title','ewa-elementor-pikme'),
			]
		);
				
		// About Me Description
		$this->add_control(
		    'ewa_about_me_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter About Me Description','ewa-elementor-pikme'),
			]
		);
		
		// About Me Details Title1
		$this->add_control(
		    'ewa_about_me_details_title1',
			[
			    'label' => esc_html__('About Me Details','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Details Title1','ewa-elementor-pikme'),
			]
		);
		
		
		// About Me Name
		$this->add_control(
		    'ewa_about_me_name',
			[
			    'label' => esc_html__('About Me Name','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Name','ewa-elementor-pikme'),
			]
		);
		
		// About Me Details Title2
		$this->add_control(
		    'ewa_about_me_details_title2',
			[
			    'label' => esc_html__('About Me Details','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Details Title2','ewa-elementor-pikme'),
			]
		);
		
		// About Me Email
		$this->add_control(
		    'ewa_about_me_email',
			[
			    'label' => esc_html__('About Me Email','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Email','ewa-elementor-pikme'),
			]
		);
		
		// About Me Email Link
		$this->add_control(
        	'ewa_about_me_email_link',
			[
				'label'         => esc_html__('About Me Email Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// About Me Details Title3
		$this->add_control(
		    'ewa_about_me_details_title3',
			[
			    'label' => esc_html__('About Me Details','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Details Title3','ewa-elementor-pikme'),
			]
		);

		// About Me Age
		$this->add_control(
		    'ewa_about_me_age',
			[
			    'label' => esc_html__('About Me Age','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Age','ewa-elementor-pikme'),
			]
		);
		
		// About Me Details Title4
		$this->add_control(
		    'ewa_about_me_details_title4',
			[
			    'label' => esc_html__('About Me Details','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Details Title4','ewa-elementor-pikme'),
			]
		);
		
		// About Me Phone Number
		$this->add_control(
		    'ewa_about_me_phone',
			[
			    'label' => esc_html__('About Me Phone Number','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Phone Number','ewa-elementor-pikme'),
			]
		);
		
		// About Me Phone Number Link
		$this->add_control(
        	'ewa_about_me_phone_link',
			[
				'label'         => esc_html__('About Me Phone Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		
		// About Me Details Title5
		$this->add_control(
		    'ewa_about_me_details_title5',
			[
			    'label' => esc_html__('About Me Details','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Details Title5','ewa-elementor-pikme'),
			]
		);
		
		
		// About Me Job
		$this->add_control(
		    'ewa_about_me_job',
			[
			    'label' => esc_html__('About Me Job','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Job','ewa-elementor-pikme'),
			]
		);
		
		// About Me Details Title6
		$this->add_control(
		    'ewa_about_me_details_title6',
			[
			    'label' => esc_html__('About Me Details','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Details Title6','ewa-elementor-pikme'),
			]
		);
		
		
		// About Me Location
		$this->add_control(
		    'ewa_about_me_location',
			[
			    'label' => esc_html__('About Me Location','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Location','ewa-elementor-pikme'),
			]
		);
		
		// About Me Button1 Text
        $this->add_control(
        	'ewa_about_me_button1_text',
			[
				'label'         => esc_html__('About Me Button Text', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button1 Text','ewa-elementor-pikme'),
			]
        );
		
		//About Me Button1 Link
		$this->add_control(
		    'ewa_about_me_button1_link',
			[
			    'label'         => esc_html__('About Me Button1 Link','ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		// About Me Button2 Text
        $this->add_control(
        	'ewa_about_me_button2_text',
			[
				'label'         => esc_html__('About Me Button Text', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button2 Text','ewa-elementor-pikme'),
			]
        );
		
		//About Me Button2 Link
		$this->add_control(
		    'ewa_about_me_button2_link',
			[
			    'label'         => esc_html__('About Me Button2 Link','ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// About Image Options
		$this->add_control(
			'ewa_about_image_options',
			[
				'label' => esc_html__( 'About Image', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Image Border Color
		$this->add_control(
			'ewa_about_image_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me__image:after' => 'border-color: {{VALUE}}',
				],
			]
		);
		
		// About Social Icon Options
		$this->add_control(
			'ewa_about_social_icon_options',
			[
				'label' => esc_html__( 'Social Icon', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// About Social Icon Background Color
		$this->add_control(
			'ewa_about_social_icon_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me__socialicon a' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// About Social Icon Color
		$this->add_control(
			'ewa_about_social_icon_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .about-me__socialicon a' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Subtitle Options
		$this->add_control(
			'ewa_about_sub_options',
			[
				'label' => esc_html__( 'Subtitle', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Subtitle Color
		$this->add_control(
			'ewa_about_sub_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me__subtitle' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Subtitle Background Color
		$this->add_control(
			'ewa_about_sub_background_color',
			[
				'label' => esc_html__( 'Separator Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me__subtitle:after' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// About Title Options
		$this->add_control(
			'ewa_about_title_options',
			[
				'label' => esc_html__( 'About Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Title Color
		$this->add_control(
			'ewa_about_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-me__title' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Text Options
		$this->add_control(
			'ewa_about_text_options',
			[
				'label' => esc_html__( 'About Text', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Text Color
		$this->add_control(
			'ewa_about_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-me p' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Text Link Color
		$this->add_control(
			'ewa_about_text_link_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-me p a' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Text Border Top
		$this->add_control(
			'ewa_about_text_border_top',
			[
				'label' => esc_html__( 'Border-top', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => 'rgba(0,0,0,.1)',
				'selectors' => [
					'{{WRAPPER}} .about-me hr' => 'border-top: 1px solid {{VALUE}}',
				],
			]
		);
		
		// About Contactinfo Options
		$this->add_control(
			'ewa_about_contactinfo_options',
			[
				'label' => esc_html__( 'About Contactinfo', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Contactinfo Color
		$this->add_control(
			'ewa_about_contactinfo_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .about-me__contactinfo span' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Button Options
		$this->add_control(
			'ewa_about_btn_options',
			[
				'label' => esc_html__( 'About Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Button Color
		$this->add_control(
			'ewa_about_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Button Border
		$this->add_control(
			'ewa_about_btn_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'border: 2px solid {{VALUE}}',
				],
			]
		);
		
		// About Button Background Color
		$this->add_control(
			'ewa_about_btn_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// About Button Before Background
		$this->add_control(
			'ewa_about_btn_before_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn:before' => 'background: {{VALUE}}',
				],
			]
		);
		
		// About Button After Background
		$this->add_control(
			'ewa_about_btn_after_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn:after' => 'background: {{VALUE}}',
				],
			]
		);
		
		// About Button Span Color
		$this->add_control(
			'ewa_about_btn_span_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn span' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Button Two Options
		$this->add_control(
			'ewa_about_btn2_options',
			[
				'label' => esc_html__( 'About Button Two', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Button Two Color
		$this->add_control(
			'ewa_about_btn2_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .site-btn.btn-line' => 'color: {{VALUE}}',
				],
			]
		);
		
		// About Button Two Background
		$this->add_control(
			'ewa_about_btn2_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .site-btn.btn-line' => 'background: {{VALUE}}',
				],
			]
		);
		
		// About Button  Two Border
		$this->add_control(
			'ewa_about_btn2_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .site-btn.btn-line' => 'border: 2px solid {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);

        // About Social Icon Hover Options
        $this->add_control(
			'ewa_social_icon_hover_options',
			[
				'label' => esc_html__( 'Social Icon', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

        // About Social Icon Hover Background Color
		$this->add_control(
			'ewa_about_social_icon_hover_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => 'transparent',
				'selectors' => [
					'{{WRAPPER}} .about-me__socialicon a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		// About Social Icon Hover Text Color
		$this->add_control(
			'ewa_about_social_icon_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me__socialicon a:hover' => 'color: {{VALUE}}',
				],
			]
		);

		// About Social Icon Hover Border Color
		$this->add_control(
			'ewa_about_social_icon_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me__socialicon a:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

        // About Text Hover Options
		$this->add_control(
			'ewa_about_text_hover_options',
			[
				'label' => esc_html__( 'About Text', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Text Hover Color
		$this->add_control(
			'ewa_about_text_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .about-me p a:hover' => 'color: {{VALUE}}',
				],
			]
		);

        // About Button Hover Options
		$this->add_control(
			'ewa_about_btn_hover_options',
			[
				'label' => esc_html__( 'About Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Button Hover Color
		$this->add_control(
			'ewa_about_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .btn:hover span' => 'color: {{VALUE}}',
				],
			]
		);

         // About Button Two Hover Options
		$this->add_control(
			'ewa_about_btn2_hover_options',
			[
				'label' => esc_html__( 'About Button Two', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Button Two Hover Color
		$this->add_control(
			'ewa_about_btn2_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .site-btn.btn-line:hover' => 'color: {{VALUE}}',
				],
			]
		);		

		// About Button Two Hover Background Color
		$this->add_control(
			'ewa_about_btn2_hover_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .site-btn.btn-line:hover' => 'background-color: {{VALUE}}',
				],
			]
		);		

		// About Button Two Hover Border Color
		$this->add_control(
			'ewa_about_btn2_hover_border_color',
			[
				'label' => esc_html__( 'Border Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .site-btn.btn-line:hover' => 'border-color: {{VALUE}}',
				],
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render about us widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$about_me_image = $settings['ewa_about_me_image']['url'];
		$about_me_facebook_link = $settings['ewa_about_me_facebook_link']['url'];
		$about_me_facebook_icon = $settings['ewa_about_me_facebook_icon_code'];
		$about_me_twitter_link = $settings['ewa_about_Me_twitter_link']['url'];
		$about_me_twitter_icon = $settings['ewa_about_me_twitter_icon_code'];
		$about_me_instagram_link = $settings['ewa_about_me_instagram_link']['url'];
		$about_me_instagram_icon = $settings['ewa_about_me_instagram_icon_code'];
		$about_me_linkedin_link = $settings['ewa_about_me_linkedin_link']['url'];
		$about_me_linkedin_icon = $settings['ewa_about_me_linkedin_icon_code'];
		$about_me_sub = $settings['ewa_about_me_sub'];
		$about_me_title = $settings['ewa_about_me_title'];
		$about_me_des = $settings['ewa_about_me_des'];
		$about_me_details_title1 = $settings['ewa_about_me_details_title1'];
		$about_me_name = $settings['ewa_about_me_name'];
		$about_me_details_title2 = $settings['ewa_about_me_details_title2'];
		$about_me_email = $settings['ewa_about_me_email'];
		$about_me_email_link = $settings['ewa_about_me_email_link']['url'];
		$about_me_details_title3 = $settings['ewa_about_me_details_title3'];
		$about_me_date_of_birth = $settings['ewa_about_me_age'];
		$about_me_details_title4 = $settings['ewa_about_me_details_title4'];
		$about_me_phone_number = $settings['ewa_about_me_phone'];
		$about_me_phone_link = $settings['ewa_about_me_phone_link']['url'];
		$about_me_details_title5 = $settings['ewa_about_me_details_title5'];
		$about_me_job = $settings['ewa_about_me_job'];
		$about_me_details_title6 = $settings['ewa_about_me_details_title6'];
		$about_me_location = $settings['ewa_about_me_location'];
		$about_me_button1_link = $settings['ewa_about_me_button1_link']['url'];
		$about_me_button1_text = $settings['ewa_about_me_button1_text'];
		$about_me_button2_text = $settings['ewa_about_me_button2_text'];
		$about_me_button2_link = $settings['ewa_about_me_button2_link']['url'];
		
       ?>
		<!-- About Area Start Here -->		
		<div class="grid">
			<div class="about-me"> 
			    <div class="col-sm-5">
				    <figure class="about-me__image">
					    <img src="<?php echo $about_me_image; ?>" alt="" />
						<div class="about-me__socialicon">
                            <a href="<?php echo esc_url($about_me_facebook_link); ?>"><?php echo $about_me_facebook_icon; ?></a>
                            <a href="<?php echo esc_url($about_me_twitter_link); ?>"><?php echo $about_me_twitter_icon; ?></a>
                            <a href="<?php echo esc_url($about_me_instagram_link); ?>"><?php echo $about_me_instagram_icon; ?></a>
                            <a href="<?php echo esc_url($about_me_linkedin_link); ?>"><?php echo $about_me_linkedin_icon; ?></a>
                        </div>
				    </figure>  <!-- about-me__image end here -->
			    </div> <!-- col-lg end here -->
				<div class="col-sm-7">
					<h5 class="about-me__subtitle"><?php echo $about_me_sub;?> </h5>
					<h2 class="about-me__title"><?php echo $about_me_title;?></h2>
					<p><?php echo $about_me_des;?></p>
  					<hr>
                  	<div class="about-me__contactinfo">
	                 	<div class="grid grid-bleed">
		                    <div class="col-sm-6 col-md-4">
		                       <span>
		                          <?php echo $about_me_details_title1;?>
		                       </span>
		                       <p><?php echo $about_me_name;?></p>
		                    </div>
		                    <div class="col-sm-6 col-md-4">
		                       <span>
		                          <?php echo $about_me_details_title2;?>
		                       </span>
		                       <p><a href="mailto:<?php echo $about_me_email_link;?>"><?php echo $about_me_email;?></a></p>
		                    </div>
		                    <div class="col-sm-6 col-md-4">
		                       <span>
		                          <?php echo $about_me_details_title3;?>
		                       </span>
		                       <p><?php echo $about_me_date_of_birth;?></p>
		                    </div>
		                    <div class="col-sm-6 col-md-4">
		                       <span>
		                          <?php echo $about_me_details_title4;?>
		                       </span>
		                       <p><a href="tel:<?php echo $about_me_phone_link;?>"><?php echo $about_me_phone_number;?></a></p>
		                    </div>
		                    <div class="col-sm-6 col-md-4">
		                       <span>
		                          <?php echo $about_me_details_title5;?>
		                       </span>
		                       <p><?php echo $about_me_job;?></p>
		                    </div>
		                    <div class="col-sm-6 col-md-4">
		                       <span>
		                          <?php echo $about_me_details_title6;?>
		                       </span>
		                       <p><?php echo $about_me_location;?></p>
		                    </div>
	                 	</div> <!-- grid end here -->
                 	</div> <!-- about-me__contactinfo end here -->
                 	<a href="<?php echo esc_url($about_me_button1_link);?>" class="btn"><span><?php echo $about_me_button1_text;?></span></a>
					<a href="<?php echo esc_url($about_me_button2_link);?>" class="site-btn btn-line"><?php echo $about_me_button2_text;?></a>
				</div> <!-- col-lg end here -->
			</div> <!-- about-me end here -->
		</div> <!-- grid end here -->
		<!-- About Area End Here -->
       <?php
	}
}