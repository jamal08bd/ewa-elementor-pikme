<?php
/**
 * EWA Elementor Slider Widget.
 *
 * Elementor widget that inserts slider into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Intro_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Slider widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-intro-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve slider widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Intro', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-sliders-h';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register slider widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-pikme'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Intro Title
		$this->add_control(
		    'ewa_intro_title',
			[
			    'label' => esc_html__('Intro Title','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Intro Title','ewa-elementor-pikme'),
			]
		);
		
		// Intro Subtitle One
		$this->add_control(
		    'ewa_intro_sub1',
			[
			    'label' => esc_html__('Intro Subtitle 1','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Intro Subtitle One','ewa-elementor-pikme'),
			]
		);
		
		// Intro Subtitle Two
		$this->add_control(
		    'ewa_intro_sub2',
			[
			    'label' => esc_html__('Intro Subtitle 2','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Intro Subtitle Two','ewa-elementor-pikme'),
			]
		);
		
		// Intro Description
		$this->add_control(
		    'ewa_intro_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Intro Description','ewa-elementor-pikme'),
			]
		);
		
		//Intro Button Link
		$this->add_control(
		    'ewa_intro_button_link',
			[
			    'label'         => esc_html__('Intro Button Link','ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		// Intro Button Text
        $this->add_control(
        	'ewa_intro_button_text',
			[
				'label'         => esc_html__('Intro Button Text', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button Text','ewa-elementor-pikme'),
			]
        );
		
		// Intro Image
		$this->add_control(
		    'ewa_intro_image',
			[
			    'label' => esc_html__('Choose Intro Image','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Intro Title Options
		$this->add_control(
			'ewa_intro_title_options',
			[
				'label' => esc_html__( 'Intro Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Intro Title Color
		$this->add_control(
			'ewa_intro_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .intro-block__title' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Intro Subtitle Options
		$this->add_control(
			'ewa_intro_sub_options',
			[
				'label' => esc_html__( 'Intro Subtitle', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Intro Subtitle One Color
		$this->add_control(
			'ewa_intro_subtitle_color',
			[
				'label' => esc_html__( 'Subtitle One Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .intro-block__subtitle' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Intro Subtitle Two Color
		$this->add_control(
			'ewa_intro_sub2_color',
			[
				'label' => esc_html__( 'Subtitle Two Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .intro-block__sub2' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Intro Description Options
		$this->add_control(
			'ewa_intro_des_options',
			[
				'label' => esc_html__( 'Intro Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Intro Description Color
		$this->add_control(
			'ewa_intro_des_color',
			[
				'label' => esc_html__( 'Description Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .intro-block__des' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Intro Button Outline Options
		$this->add_control(
			'ewa_intro_button_text_options',
			[
				'label' => esc_html__( 'Button Outline', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Intro Button Outline Color 
		$this->add_control(
			'ewa_intro_button_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn-outline' => 'color: {{VALUE}}',
			   ],
			]
		);

		// Intro Button Outline Background
		$this->add_control(
			'ewa_intro_button_text_background',
			[
			  'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
			  'type' => \Elementor\Controls_Manager::COLOR,
			  'scheme' => [
				  'type' => \Elementor\Core\Schemes\Color::get_type(),
				  'value' => \Elementor\Core\Schemes\Color::COLOR_1,
			  ],
			  'default' => '#f75958',
			  'selectors' => [
				  '{{WRAPPER}} .btn-outline' => 'background-color: {{VALUE}}',
			  ],
			]
		);
		
		// Intro Button Options
		$this->add_control(
			'ewa_intro_button_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Intro Button Background
		$this->add_control(
			'ewa_intro_button_background',
			[
			  'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
			  'type' => \Elementor\Controls_Manager::COLOR,
			  'scheme' => [
				  'type' => \Elementor\Core\Schemes\Color::get_type(),
				  'value' => \Elementor\Core\Schemes\Color::COLOR_1,
			  ],
			  'default' => '#f75958',
			  'selectors' => [
				  '{{WRAPPER}} .intro-block__button:before' => 'background-color: {{VALUE}}',
			  ],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);

        // Intro Button Hover Options
		$this->add_control(
			'ewa_intro_btn_hover_options',
			[
				'label' => esc_html__( 'Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Intro Button Hover Background Color
		$this->add_control(
			'ewa_intro_btn_hover_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => 'transparent',
				'selectors' => [
					'{{WRAPPER}} .intro-block__button:hover' => 'background-color: {{VALUE}}',
				],
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render slider widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$intro_title = $settings['ewa_intro_title'];
		$intro_subtitle_one = $settings['ewa_intro_sub1'];
		$intro_subtitle_two = $settings['ewa_intro_sub2'];
		$intro_des = $settings['ewa_intro_des'];
		$intro_btn_link = $settings['ewa_intro_button_link']['url'];
		$intro_btn_text = $settings['ewa_intro_button_text'];
		$intro_image = $settings['ewa_intro_image']['url'];

		?>
			<!-- Slider Start Here -->	
			    <div class="intro-block">
			        <div class="grid align-center">
					    <div class="col-xl-6 col-sm-7">
					        <div class="intro-block__content">
						        <h2 class="intro-block__title"><?php echo $intro_title;?></h2>
								<h4 class="intro-block__subtitle"><?php echo $intro_subtitle_one;?>
								    <span class="intro-block__sub2"><?php echo $intro_subtitle_two;?></span>
								</h4>
								<p class="intro-block__des"><?php echo $intro_des;?></p>
								<a href="<?php echo esc_url($intro_btn_link);?>" class="btn btn-outline intro-block-button"><?php echo $intro_btn_text;?></a>
						    </div> <!-- intro-block__content end here -->
			            </div> <!-- col-md end here -->
						<div class="col-xl-5 col-sm-5 padding-bottom-5">
						    <div class="intro-block__image">
							    <img src="<?php echo $intro_image; ?>" alt="" />
							</div> 
						</div><!-- col-md end here -->
					</div>	<!-- intro-block end here -->
			    </div> <!-- grid end here -->
			<!-- Slider End Here -->			
		<?php 
	} 
}