<?php
/**
 * EWA Elementor Contact Details Widget.
 *
 * Elementor widget that inserts a contact into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Contact_Details_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve contact details widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-contact-details-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve contact details widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Contact Details', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve contact details widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-envelope';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the contact details widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register contact details widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-pikme'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Contact Title
		$this->add_control(
		    'ewa_contact_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Title','ewa-elementor-pikme'),
			]
		);
		
		// Contact Description
		$this->add_control(
		    'ewa_contact_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Description','ewa-elementor-pikme'),
			]
		);


		// Contact Items Repeater
		$repeater = new \Elementor\Repeater();
		
		// Contact Item Title
		$repeater->add_control(
			'ewa_contact_item_title',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Title' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Contact Item Description
		$repeater->add_control(
			'ewa_contact_item_desc',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Description' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Contact Item Icon
		$repeater->add_control(
			'ewa_contact_item_icon',
			[
				'label' => esc_html__( 'Icon', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Icon' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Contact List
		$this->add_control(
			'ewa_contact_list',
			[
				'label' => esc_html__( 'Contact Items', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_contact_item_title }}}',
			]
		);

		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Contact Title Options
		$this->add_control(
			'ewa_contact_title_options',
			[
				'label' => esc_html__( 'Contact Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Contact Title Color
		$this->add_control(
			'ewa_contact_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .contact-me__title' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Contact Description Options
		$this->add_control(
			'ewa_contact_des_options',
			[
				'label' => esc_html__( 'Contact Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Contact Description Color
		$this->add_control(
			'ewa_contact_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .contact-me__description' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Contact Item Options
		$this->add_control(
			'ewa_contact_item_options',
			[
				'label' => esc_html__( 'Contact item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Contact Item Color
		$this->add_control(
			'ewa_contact_item_icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .contact-me__item p i' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Contact Item Title Color
		$this->add_control(
			'ewa_contact_item_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .contact-me__item p.contact-me__text' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Contact Item Des Color
		$this->add_control(
			'ewa_contact_item_des_color',
			[
				'label' => esc_html__( 'Description Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .contact-me__item p' => 'color: {{VALUE}} !important',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render contact details widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$contact_title = $settings['ewa_contact_title'];
		$contact_description = $settings['ewa_contact_des'];		
       ?>

		<div class="contact-me">
			<h5 class="contact-me__title"><?php echo $contact_title;?></h5>
			<p class="contact-me__description"><?php echo $contact_description;?> </p>
		    <div class="contact-me__info">
				<div class="contact-me__item">
					<?php 
				        foreach (  $settings['ewa_contact_list'] as $item ) { 
					        $contact_title = $item['ewa_contact_item_title'];
					        $contact_desc = $item['ewa_contact_item_desc'];
					        $contact_icon =  $item['ewa_contact_item_icon'];
				        ?>						
						<div class="contact-me__item">
							<p class="contact-me__text"><?php echo $contact_title;?> </p>
							<p><?php echo $contact_icon; ?><?php echo $contact_desc; ?></p>
						</div>
					<?php } ?>
				</div>
			</div> <!-- contact-me-info end here -->
		</div><!-- contact end here -->

       <?php
	}
}