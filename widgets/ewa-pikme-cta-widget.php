<?php
/**
 * EWA Elementor CTA Widget.
 *
 * Elementor widget that inserts CTA into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Cta_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve about us widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-cta-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve about us widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme CTA', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve about us widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the about us widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register about us widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-pikme'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// CTA Image
		$this->add_control(
		    'ewa_cta_image',
			[
			    'label' => esc_html__('Choose CTA Image','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// CTA Title
		$this->add_control(
		    'ewa_cta_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter CTA Title','ewa-elementor-pikme'),
			]
		);
		
		// CTA Description
		$this->add_control(
		    'ewa_cta_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter CTA Description','ewa-elementor-pikme'),
			]
		);
		
		// CTA Button Text
        $this->add_control(
        	'ewa_cta_button_text',
			[
				'label'         => esc_html__('Button Text', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Click here','ewa-elementor-pikme'),
			]
        );
		
		//CTA Link
		$this->add_control(
		    'ewa_cta_link',
			[
			    'label'         => esc_html__('CTA Link','ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// CTA Title Options
		$this->add_control(
			'ewa_cta_title_options',
			[
				'label' => esc_html__( 'CTA Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Title Color
		$this->add_control(
			'ewa_cta_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cta-block__title' => 'color: {{VALUE}}',
				],
			]
		);
		
		// CTA Description Options
		$this->add_control(
			'ewa_cta_des_options',
			[
				'label' => esc_html__( 'CTA Description', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Description Color
		$this->add_control(
			'ewa_cta_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cta-block__des' => 'color: {{VALUE}}',
				],
			]
		);
		
		// CTA Button Options
		$this->add_control(
			'ewa_cta_btn_options',
			[
				'label' => esc_html__( 'CTA Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// CTA Button Color
		$this->add_control(
			'ewa_cta_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .cbtn2' => 'color: {{VALUE}}',
				],
			]
		);
		
		// CTA Button Background
		$this->add_control(
			'ewa_cta_btn_bachground_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cbtn2' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// CTA Border
		$this->add_control(
			'ewa_cta_btn_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .cbtn2' => 'border: 1px solid {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);

        // CTA Button Options
		$this->add_control(
			'ewa_cta_btn_hover_options',
			[
				'label' => esc_html__( 'CTA Button', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Service Button Background
		$this->add_control(
			'ewa_cta_btn_hover_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .cbtn2:hover' => 'background-color: {{VALUE}}',
				],
			]
		);	

		// Service Button Color
		$this->add_control(
			'ewa_cta_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .cbtn2:hover' => 'color: {{VALUE}}',
				],
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render about us widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$cta_image = $settings['ewa_cta_image']['url'];
		$cta_title = $settings['ewa_cta_title'];
		$cta_description = $settings['ewa_cta_des'];
		$cta_btn_text = $settings['ewa_cta_button_text'];
		$cta_link = $settings['ewa_cta_link']['url'];
		
       ?>
		<!-- CTA Area Start Here -->		
		    <div class="cta-block">
                <div class="grid align-center">
                    <div class=".col-md-9 col-sm-8">
                        <div class="cta-block__text">
                            <img src="<?php echo $cta_image; ?>" />
                            <h4 class="cta-block__title"><?php echo $cta_title;  ?></h4>
                            <p class= "cta-block__des"><?php echo $cta_description;  ?></p>
                        </div>
                    </div>
                    <div class="cl-md-3 col-sm-4">
                        <div class="cta-block__btn">
                           <a href="<?php echo esc_url($cta_link); ?>" class="cbtn2"><?php echo $cta_btn_text; ?></a>
                        </div>
                    </div>
                </div> <!-- grid end here -->
            </div> <!-- cta-block end here -->
		<!-- CTA Area End Here -->
       <?php
	}
}