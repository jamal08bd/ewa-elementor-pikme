<?php
/**
 * EWA Elementor Heading Widget.
 *
 * Elementor widget that inserts a heading into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Skills_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-skills-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve heading widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Skills', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-heading';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the heading widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		// Skills Repeater
		$repeater = new \Elementor\Repeater();
		
		// Skills Name
		$repeater->add_control(
			'ewa_skills_name',
			[
				'label' => esc_html__( 'Skills Name', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Skills Name' , 'ewa-elementor-pikme' ),
			]
		);

		// Skills Percent
		$repeater->add_control(
			'ewa_skills_percent',
			[
				'label' => esc_html__( 'Percent', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Skills Percent' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Skills List
		$this->add_control(
			'ewa_skills_list',
			[
				'label' => esc_html__( 'Skills List', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_skills_name }}}',
			]
		);
		
		// Skills Repeater
		$repeater = new \Elementor\Repeater();
		
		// Skills Name
		$repeater->add_control(
			'ewa_skills_name_right',
			[
				'label' => esc_html__( 'Skills Name Right', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Skills Name Right' , 'ewa-elementor-pikme' ),
			]
		);

		// Skills Percent
		$repeater->add_control(
			'ewa_skills_percent_right',
			[
				'label' => esc_html__( 'Percent Right', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Skills Percent Right' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Skills List Right
		$this->add_control(
			'ewa_skills_list_right',
			[
				'label' => esc_html__( 'Skills List Right', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_skills_name_right }}}',
			]
		);

		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Skills Text Options
		$this->add_control(
			'ewa_skills_text_options',
			[
				'label' => esc_html__( 'Text', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Skills Text Color
		$this->add_control(
			'ewa_skills_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .skill-block__text' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Skills Value Options
		$this->add_control(
			'ewa_skills_value_options',
			[
				'label' => esc_html__( 'Value', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Skills Value Color
		$this->add_control(
			'ewa_skills_value_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .skill-block__value' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Skills Wrapper Options
		$this->add_control(
			'ewa_skills_wrapper_options',
			[
				'label' => esc_html__( 'Wrapper', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Skills Wrapper Color
		$this->add_control(
			'ewa_skills_wrapper_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#e8e8e8',
				'selectors' => [
					'{{WRAPPER}} .skill-block__wrapper' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Skills Inner Options
		$this->add_control(
			'ewa_skills_inner_options',
			[
				'label' => esc_html__( 'Inner', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Skills Inner Color
		$this->add_control(
			'ewa_skills_inner_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .skill-block__inner' => 'background: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render heading widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
       ?>
		<!-- Section Title Start Here -->
			
			<div class="grid">
			    <div class="col-sm-6">
                    <div class="skill-block">
					    <?php 
				        foreach (  $settings['ewa_skills_list'] as $item ) { 

					        $skills_name = $item['ewa_skills_name'];
					        $skills_percent = $item['ewa_skills_percent'];
				        ?>
						
						    <p class="skill-block__text">
						        <?php echo $skills_name; ?>
							    <span class="skill-block__value"><?php echo $skills_percent; ?></span>
						    </p> <!-- skill-block__text end here -->
						
						    <div class="skill-block__wrapper">
						        <div class="skill-block__inner" style="width: <?php echo $skills_percent; ?> ">
							        <p class="skill-block__proportion"></p>
							    </div>
						    </div> <!-- skill-block__wrapper end here -->
						<?php } ?>						
						
                    </div>	<!-- skill-block end here -->			
				</div>	<!-- col. end here -->	
                <div class="col-sm-6">
                    <div class="skill-block">
					    <?php 
				        foreach (  $settings['ewa_skills_list_right'] as $item ) { 

					        $skills_name_right = $item['ewa_skills_name_right'];
					        $skills_percent_right = $item['ewa_skills_percent_right'];
				        ?>
						
						    <p class="skill-block__text">
						        <?php echo $skills_name_right; ?>
							    <span class="skill-block__value"><?php echo $skills_percent_right; ?></span>
						    </p> <!-- skill-block__text end here -->
						
						    <div class="skill-block__wrapper">
						        <div class="skill-block__inner" style="width: <?php echo $skills_percent_right; ?> ">
							        <p class="skill-block__proportion"></p>
							    </div>
						    </div> <!-- skill-block__wrapper end here -->
						<?php } ?>						
						
                    </div>	<!-- skill-block end here -->			
				</div>	<!-- col. end here -->				
			</div> <!-- grid end here -->
			
		<!-- Section Title End Here -->
       <?php
	}
}