<?php
/**
 * EWA Elementor Statistics Widget.
 *
 * Elementor widget that inserts statistics into the page
 *
 * @since 1.0.0
 */
class EWA_Statistics_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve statistics widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-statistics-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve statistics widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Statistics', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve statistics widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-sort-amount-up';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the statistics widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register statistics widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

	}

	/**
	 * Render statistics widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
       ?>
		<!-- Statistics Area Start Here -->
		 <h3>HTML GOES HERE</h3>
		<!-- Statistics Area End Here -->
       <?php
	}
}