<?php
/**
 * EWA Elementor Price Plan Widget.
 *
 * Elementor widget that inserts price plan into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Price_Plan_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve price plan widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-price-plan-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve price plan widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Price Plan', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve price plan widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-money-bill-alt';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the price plan widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register price plan widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		//Price Plan Icon Code
		$this->add_control(
		    'ewa_price_plan_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-pikme'),
			]
		);
		
		// Price Plan Name
		$this->add_control(
			'ewa_price_plan_name',
			[
				'label' => esc_html__( 'Name', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
                'placeholder' => esc_html__( 'Enter Price Plan Name', 'ewa-elementor-pikme' ),
			]
		);
		
		// Price Plan Currency
		$this->add_control(
			'ewa_price_plan_currency',
			[
				'label' => esc_html__( 'Currency', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
                'placeholder' => esc_html__( 'Enter Currency', 'ewa-elementor-pikme' ),
			]
		);

        // Price Plan Title
		$this->add_control(
			'ewa_price_plan_title',
			[
				'label' => esc_html__( 'Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
                'placeholder' => esc_html__( 'Enter Title', 'ewa-elementor-pikme' ),
			]
		);
		
		// Price Plan Unit
		$this->add_control(
			'ewa_price_plan_unit',
			[
				'label' => esc_html__( 'Unit', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter Unit', 'ewa-elementor-pikme' ),
			]
		);

		$repeater = new \Elementor\Repeater();

		// Repeater for Features Title
		$repeater->add_control(
			'ewa_price_plan_features',
			[
				'label' => esc_html__( 'Features Title', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Add New Feature' , 'ewa-elementor-pikme' ),
			]
		);

		// Features List
		$this->add_control(
			'ewa_price_plan_features_list',
			[
				'label' => esc_html__( 'Features List', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_price_plan_features }}}',
			]
		);

		// Price Plan Button Text
        $this->add_control(
        	'ewa_price_plan_button_text',
			[
				'label'         => esc_html__('Price Plan Button Text', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Click here','ewa-elementor-pikme'),
			]
        );
		
		// Price Plan Button Link
        $this->add_control(
        	'ewa_price_plan_button_link',
			[
				'label'         => esc_html__('Price Plan Button Link', 'ewa-elementor-pikme'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
					'url'   => '#',
				],
			]
        );
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Price Plan Icon Options
		$this->add_control(
			'ewa_price_plan_icon_options',
			[
				'label' => esc_html__( 'Price Plan Icon', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Price Plan Icon Color
		$this->add_control(
			'ewa_price_plan_icon_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .price-block__item i' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Icon Background
		$this->add_control(
			'ewa_price_plan_icon_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .price-block__item i' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Icon Border
		$this->add_control(
			'ewa_price_plan_icon_border',
			[
				'label' => esc_html__( 'Border', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#EFEFEF',
				'selectors' => [
					'{{WRAPPER}} .price-block__item i' => 'border: 1px solid {{VALUE}}',
				],
			]
		);
		
		// Price Plan Name Options
		$this->add_control(
			'ewa_price_plan_name_options',
			[
				'label' => esc_html__( 'Price Plan Name', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Price Plan Name Color
		$this->add_control(
			'ewa_price_plan_name_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .price-block__plan' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Value Options
		$this->add_control(
			'ewa_price_plan_value_options',
			[
				'label' => esc_html__( 'Value Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Price Plan Value Color
		$this->add_control(
			'ewa_price_plan_value_color',
			[
				'label' => esc_html__( 'Amount Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .price-block__value' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Value Unit Color
		$this->add_control(
			'ewa_price_plan_value_unit_color',
			[
				'label' => esc_html__( 'Unit Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .price-block__unit' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Content Options
		$this->add_control(
			'ewa_price_plan_content_options',
			[
				'label' => esc_html__( 'Content Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Price Plan Content Color
		$this->add_control(
			'ewa_price_plan_content_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .price-block__content ul li' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Button Options
		$this->add_control(
			'ewa_price_plan_button_options',
			[
				'label' => esc_html__( 'Button Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Price Plan Button Color
		$this->add_control(
			'ewa_price_plan_button_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .price-block__button' => 'color: {{VALUE}}',
				],
			]
		);

		// Price Plan Button Background
		$this->add_control(
			'ewa_price_plan_button_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .price-block .btn.btn-outline.intro-block-button' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Button Background Before
		$this->add_control(
			'ewa_price_plan_button_background_before',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .intro-block-button::before' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);

        // Price Plan Button Hover Options
		$this->add_control(
			'ewa_price_plan_button_hover_options',
			[
				'label' => esc_html__( 'Button Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		
		// Price Plan Button Color
		$this->add_control(
			'ewa_price_plan_button_hover_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .price-block__button:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Price Plan Button Background
		$this->add_control(
			'ewa_price_plan_button_hover_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#343a40',
				'selectors' => [
					'{{WRAPPER}} .price-block__button:hover' => 'background: {{VALUE}}',
				],
			]
		);	

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render price plan widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$price_plan_icon = $settings['ewa_price_plan_icon_code'];
		$price_plan_name = $settings['ewa_price_plan_name'];
		$price_plan_currency = $settings['ewa_price_plan_currency'];
		$price_plan_title = $settings['ewa_price_plan_title'];
		$price_plan_unit = $settings['ewa_price_plan_unit'];
		$price_plan_button_text = $settings['ewa_price_plan_button_text'];
		$price_plan_button_link = $settings['ewa_price_plan_button_link']['url'];

       ?>
		<!-- Pricing plan Area Start Here -->
			<div class="price-block">
			    <div class="price-block__item">
				    <?php echo $price_plan_icon; ?>
				    <span class="price-block__plan"><?php echo $price_plan_name;?></span>
				    <h3 class="price-block__value">
				       <em><?php echo $price_plan_currency;?></em>
				       <?php echo $price_plan_title;?>
				       <span class="price-block__unit"> <?php echo $price_plan_unit;?></span>
				    </h3>
				    <div class="price-block__content">
				        <ul>
					    <?php 
				        foreach (  $settings['ewa_price_plan_features_list'] as $item ) { 
					        $ewa_price_plan_features = $item['ewa_price_plan_features'];
				        ?>
					        <li class="price-area__item"><?php echo $ewa_price_plan_features;?></li>
				        <?php } ?>
				        </ul>
			        </div> <!-- price-block__content end here -->
				    <a href="<?php echo esc_url($price_plan_button_link);?>" class="btn btn-outline intro-block-button"><?php echo $price_plan_button_text;?></a>
			    </div> <!-- price-block__item end here -->
			</div> <!-- price-block end here -->
		<!-- Pricing plan Area End Here -->
       <?php
	}
}