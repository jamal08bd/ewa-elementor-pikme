<?php
/**
 * EWA Elementor Process Widget.
 *
 * Elementor widget that inserts Process into the page
 *
 * @since 1.0.0
 */
class EWA_Pikme_Process_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Process widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-pikme-process-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Process widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Pikme Process', 'ewa-elementor-pikme' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Process widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-th';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Process widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-pikme' ];
	}

	/**
	 * Register Process widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of Controls Section
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		// Work Process Repeater
		$repeater = new \Elementor\Repeater();
		
		// Work Process Steps Odd Number
		$repeater->add_control(
			'ewa_work_process_odd_no',
			[
				'label' => esc_html__( 'Number', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Steps Odd Numbers' , 'ewa-elementor-pikme' ),
			]
		);

		//Work Process Odd Number Icon Code
		$repeater->add_control(
		    'ewa_work_process_odd_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-pikme'),
			]
		);
		
		// Work Process Odd Number Steps Name
		$repeater->add_control(
			'ewa_work_process_odd_name',
			[
				'label' => esc_html__( 'Steps Name', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter  Work Process Odd Number Steps Name' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Work Process Steps Number
		$repeater->add_control(
			'ewa_work_process_even_no',
			[
				'label' => esc_html__( 'Number', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Steps Number' , 'ewa-elementor-pikme' ),
			]
		);
		
		// Work Process Steps Name
		$repeater->add_control(
			'ewa_work_process_even_name',
			[
				'label' => esc_html__( 'Steps Name', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter  Work Process Steps Name' , 'ewa-elementor-pikme' ),
			]
		);
		
		//Work Process Icon Code
		$repeater->add_control(
		    'ewa_work_process_even_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-pikme'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-pikme'),
			]
		);

		// Work Process Steps List
		$this->add_control(
			'ewa_work_process_list',
			[
				'label' => esc_html__( 'Work Process Steps List', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_work_process_odd_no }}}',
			]
		);
		

		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-pikme' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-pikme' ),
			]
		);

		// Process Item Options
		$this->add_control(
			'ewa_process_item_options',
			[
				'label' => esc_html__( 'Process Item', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Process Item Background
		$this->add_control(
			'ewa_process_item_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .process-block__item:after' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Process Number Options
		$this->add_control(
			'ewa_process_number_options',
			[
				'label' => esc_html__( 'Process Number', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Process Number Background
		$this->add_control(
			'ewa_process_number_background',
			[
				'label' => esc_html__( 'Background', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .process-block__number' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Process Number Color
		$this->add_control(
			'ewa_process_number_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .process-block__number' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Process Icon Options
		$this->add_control(
			'ewa_process_icon_options',
			[
				'label' => esc_html__( 'Process Icon', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Process Icon Color
		$this->add_control(
			'ewa_process_icon_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#f75958',
				'selectors' => [
					'{{WRAPPER}} .process-block__icon' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Process Text Options
		$this->add_control(
			'ewa_process_text_options',
			[
				'label' => esc_html__( 'Process Text', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Process Icon Color
		$this->add_control(
			'ewa_process_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-pikme' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				],
				'default' => '#333',
				'selectors' => [
					'{{WRAPPER}} .process-block__text' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-pikme' ),
			]
		);		

		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render Process widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

       ?>
		<!-- Process Start Here -->
		    <div class="process-block">
			    <div class="grid">
			        <?php 
				    foreach (  $settings['ewa_work_process_list'] as $item ) { 

					    $work_process_odd_no = $item['ewa_work_process_odd_no'];
						$work_process_odd_icon = $item['ewa_work_process_odd_icon_code'];
					    $work_process_odd_name = $item['ewa_work_process_odd_name'];
						$work_process_even_no = $item['ewa_work_process_even_no'];
						$work_process_even_name = $item['ewa_work_process_even_name'];
						$work_process_even_icon = $item['ewa_work_process_even_icon_code'];
				    ?>
					
					    <div class="col-lg-2 process-block__item align-self-start ">
                            <div class="mt-top">
					            <span class="process-block__number"><?php echo $work_process_odd_no; ?></span>
						        <span class="process-block__icon"><?php echo $work_process_odd_icon; ?></span>
						        <h3 class="process-block__text"><?php echo $work_process_odd_name; ?></h3>
					        </div> <!-- mt-bottom end here -->	
				        </div>	<!-- col-lg end here -->
						
						<div class="col-lg-2 process-block__item align-self-end ">
                            <div class="mt-bottom">
					            <span class="process-block__number process-block__bottom"><?php echo $work_process_even_no; ?></span>
						        <h3 class="process-block__text"><?php echo $work_process_even_name; ?></h3>
								<span class="process-block__icon"><?php echo $work_process_even_icon; ?></span>
					        </div> <!-- mt-bottom end here -->	
				        </div>	<!-- col-lg end here -->
						
					<?php } ?>					
                    
			    </div> <!-- grid end here -->
			</div> <!-- process-block end here -->	
		<!-- Process End Here -->
       <?php
	}
}